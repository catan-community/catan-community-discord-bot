import requests
import os

response = requests.get("https://catancommunity.org/cms/explanation/Bot-Commands",
                        headers={"Accept": "Application/JSON"})
data = response.json()

with open("readme.md", encoding='utf-8') as f:
    data["page"]["content"] = f.read()

del data["page"]["date_modified"]
del data["page"]["path"]

res = requests.post(
    "https://catancommunity.org/cms/editor/explanation/Bot-Commands",
    headers={"X-authorization": os.environ.get("AUTHENTICATION_KEY")},
    json=data["page"]
)
res.raise_for_status()
