import asyncio
import csv
import re

import db


async def load_scores_for_tournament(database, tournament_names, file_name, offset, dialect):
    with open(file_name, encoding='utf-8') as f:
        data = csv.reader(f, dialect=dialect)
        for index, row in enumerate(data):
            if index == 0:  # Ignore header row
                continue
            if len(row) > 0 and row[0].strip():
                name = row[0]
                ts = row[offset:]
                ts += ([0] * (len(tournament_names) - len(ts)))
                ts = tuple((int(i) if i else 0) for i in ts)
                print(name, ts)
                print({"tournaments.1." + name: ts[number] for number, name in enumerate(tournament_names)})

                result = await database["users"].update_one(
                    {"display_name": {
                        "$regex": '^' + re.escape(name)
                    }},
                    {
                        "$set": {"tournaments.1." + name: ts[number] for number, name in enumerate(tournament_names)}
                        #     {
                        #     "tournaments.1.open11": ts[0],
                        #     "tournaments.1.open12": ts[1],
                        #     "tournaments.1.open13": ts[2]
                        # }
                    }

                )
                if result.modified_count == 0:
                    print("Failed")


async def load_tournament_scores():
    database = db.get_db(True)

    csv.register_dialect('semicolon', delimiter=';')

    await load_scores_for_tournament(
        database,
        ('open14',),
        "CCOpen14.csv",
        2,
        'semicolon'

    )
    # await load_scores_for_tournament(database, ('open11', 'open12', 'open13'),
    #                                  "tournament_points_season_1.csv",
    #                                  1, 'excel-tab')

    # if len(user) >= 1:
    #     print(row[0], user[0].get("display_name", ""))


if __name__ == "__main__":
    asyncio.run(load_tournament_scores())
