import asyncio

import pymongo
import datetime
import json

import db


async def get_referal_leaderboard():
    database = db.get_db(True)

    result = database["user_invites"].aggregate(
        [
            {
                "$set": {
                    "user_mention": {
                        "$concat": [
                            "<@",
                            {
                                "$toString": "$user_id",
                            },
                            ">"
                        ]
                    }
                }
            },
            {
                "$lookup": {
                    "from": "users",
                    "localField": "user_mention",
                    "foreignField": "discord_username",
                    "as": "user_data"
                }
            },
            {
                "$match": {
                    "user_data": {
                        "$not": {
                            "$size": 0
                        }
                    }
                }
            },
            {
                "$lookup": {
                    "from": "user_lp",
                    "localField": "user_data.0._id",
                    "foreignField": "user",
                    "as": "user_lp"
                }
            },
            {
                "$unwind": 
                    "$user_lp"
            },
            {
                "$match": {
                    "user_lp.month": {
                        "$lt": 8
                    }
                }
            },
            {
                "$set": {
                    "is_tournament": {
                        "$cond": {
                            "if": {
                                "type": "tourney"
                            },
                            "then": 1,
                            "else": 0
                        }
                    }
                }
            },
            {
                "$group": {
                    "_id": {"discord": "$user_id"},
                    "inviter": {
                        "$first": "$inviter_id",
                    },
                    "matches": {
                        "$sum": "$user_lp.matches"
                    },
                    "tournaments": {
                        "$sum": "$is_tournament"
                    }
                }
            },
            {
                "$match": 
                    {"$or": 
                        [
                            {
                                "matches": {
                                    "$gte": 3
                                },
                            },
                            {
                                "tournaments": {
                                    "$gte": 1
                                }
                            }
                        ]
                    }  
            },
            {
                "$group": {
                    "_id": {"inviter": "$inviter"},
                    "invites": {
                        "$count": {}
                    },
                    "total_matches": {
                        "$sum": "$matches"
                    },
                    "total_tournaments": {
                        "$sum": "$tournaments"
                    }
                }
            },
            {
                "$sort": {
                    "invites": -1
                }
            }
        ]
    )

    index = 0
    async for i in result:
        index+=1
        print(f"#{index}: {i['invites']} invites by <@{i['_id']['inviter']}> (total games played by invitees: {i['total_matches']}, tournaments: {i['total_tournaments']})")


if __name__ == "__main__":
    asyncio.run(get_referal_leaderboard())
