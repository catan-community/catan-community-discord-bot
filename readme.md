# The bot

In Catan Community, games are made and tracked using our resident discord bot. If you want to play, consider picking up
a role in [#role assignment](https://discord.com/channels/894372075622526986/894372076629164046) to get a ping when
people are looking for a game. Games are not just fun, but can earn
[league points](https://catancommunity.org/cms/explanation/League-Points) and hopefully qualify for the
[end of month tournament](https://catancommunity.org/cms/Tournaments/End-of-Month).

Having trouble with the bot? Ask any CC team member and we will be happy to help. Many other community members
would be glad to help you too.

# 🎮 Joining a game 🎮

1. Get a role in [#role-assignment](https://discord.com/channels/894372075622526986/894372076629164046) so you can get
   notified when a game of your preferred type is created.
2. When a player starts a game, press the 🎮 under the bot message to join. Be sure to read any special rules the host
   has set.
3. The host should post a link to the game on colonist.io. Click the link to join the game.
4. Say Hi and be friendly to the other players.
5. After the game ends, see Reporting results to see how to report the game results.

# Creating a game

1. To start a base game with new players, type
   in [#colonist](https://discord.com/channels/894372075622526986/894372077153435674) :

```text
    !base [link to game on colonist.io]
```

Replace !base with `!ck` or `!sf` depending on the mode you want to play. If you don't provide a link one will be
created for you.

If you have a full room already create the game
in [#rematch](https://discord.com/channels/894372075622526986/894372077153435675) instead so players know not to join.

2. Tag the appropriate role. See [#role-assignment](https://discord.com/channels/894372075622526986/894372076629164046)
   for available roles. Be specific with yours pings Include the game mode, speed, any wager, and any special rules. Do
   not ping a role more than once every 5 minutes.
3. The bot will create a game for you.
4. Wait for players to react 🎮 under the bot message. Any player can join by pressing the icon.
5. When the game fills up, start the game and enjoy
6. After the game ends, see Reporting Results to see how to report the game results

## 🏆 Reporting the results🏆

1. One player takes a screenshot of the game end screen. This should include the scores of each player including hidden
   VPs
2. Post the screenshot in [#report-winner](https://discord.com/channels/894372075622526986/894372077153435676)
3. Type:

```
!win [Game id] @WinnerDiscordName
```

5. At least one other player has to react with 👍 for the win to be approved.
6. The winner will get LP for winning, other players can get LP too if it's their first game of the day.
7. (Optional) Type `!rank` in [#bot-spam](https://discord.com/channels/894372075622526986/894372077153435677) to see how
   you did.
8. Play another game

## 📈 Playing with wager📈

Want a bit more tension? Wager games can raise the stakes. Create a wager game by including a number immediately after
the bot message. For example: `!base 25`

In this case, the winner gets another 25 LP per player while everybody else loses 25 LP. Overall the LP increases the
same as a normal game.

## 📝 Other Rules📝

1. General [server rules](https://catancommunity.org/cms/explanation/Rules) apply
2. If you are creating a game while there is another game waiting for players, promote is as well in your tag. For
   example:

```text
    @Colonist 3 people needed for 4p or wager game above
```

5. Do not ping any role more than once every 5 minutes. Please also only ping the role for the type of game you are
   creating

6. Choose whatever settings you want, as long as it's not clearly intended to cheat the league point system. For example
   a game to 4vp.

## Common Bot Commands

Commands tagged with ✈️ can be sent over direct message.

Commands tagged with 🛡️ can only be used by the CC team. Want admin powers yourself, check out
[this page](https://catancommunity.org/cms/Meet-the-Team) for more info about how to join.

### `!creategame [wager]`

Make a game. React to the result to join a game. Joining a game will add 25 lp to the prize pool. Wager is optional.

You can optionally write a link after this command which will be copied to the response. Examples:

Create a base game with 0 wager:

    !base

Create a ck with 0 wager and a link:

    !ck https://colonist.io/#mouse

Create a seafarers with 25 wager: (winner of a 4 player game with 25 wager will earn 175 points)

    !sf 25

Make a game with both a wager and a link:

    !creategame 25 https://colonist.io/#mouse

Tag colonist in the same message:

    !base 25 https://colonist.io/#mouse @colonist 25 wager game join now!

### `!win [game id] [@winner]`

Finish a game. This message must include a screenshot of the final scores. All players must react to the message to
confirm.

Example:

     !winner SilverWheat242F @mousetail


### `!unconfirmed`
Shows you the games that you were in but are still unconfirmed. If you are not the winner, please confirm or tag 
the player that needs to confirm the game

For admins and members of CC team, it will show all unconfirmed games.

### `!rank` ✈️

Shows your rank and total lp. Accepts filters, for example:

    !rank C&K

Get your rank for only C&K games this month

    !rank 4p

Get your rank for only 4 player games this month

    !rank February

Get your rank for February this year

    !rank season 3p

Get your rank for 3 player games this season.

To get your rank for a specific league use `!baserank` or `!ckrank`

### `!rank [@user]`

get a specific users rank. Accepts the same filters as for `!rank`

### `!lphistory` ✈️

get your most recent LP events

### `!lphistory [@user]`

get a users LP history

### `!netwager` ✈️

get your net amount of wagered LPs

### `!netwager [@user]`

get a users net amount of wagered LPs

### `!leaderboard [time period]` ✈️

Shows the 25 best players. Accepts the same fitlers as `!rank`. For example:

    !rank C&K

Get the best cities and knights players this month

    !rank 4p all

Get the best at 4 players games in any game mode over all time

### `!help` ✈️

️Shows this page.

### `!start [game id]`

Mark a game as started, so that no further players can join.

For example:

    !start SilverWheat242F

### `!remove [game id] [user mention]`

Remove a user from a game. Please ask a player to leave first, only kick them if they won't respond.

For example:

    !remove SilverWheat298K @mousetail

## Fun Commands

### `!quote`

Learn what history can teach us about how to play catan.

If you have ideas for more quotes you can edit the
file [here](https://gitlab.com/mousetail/catan-community-discord-bot/-/blob/master/data/quotes.txt). All changes need to
be approved.

## Mod Commands

These commands require the user to be admin/mod.

### `!adminwinner [game id] [@winner]` 🛡️

Immediately closes a match and awards the specified winner VP.

Example:

    !adminwinner SilverWhat242F @Puzzles

### `!adminlp [@user] lp [games]` 🛡️

Gives \@user lp according to `lp`. You can use `!adminlp [@user] 0` to add a user to the leaderboard without giving them
any lp.

Example of giving Hippo 25 LP:

    !adminlp @Hippo 25

Add NordicZombie to the leaderboard:

    !adminlp @NordicZombie 0

Add one game and 100 LP to ReMarkable:

    !adminlp @ReMarkable 100 1

Subtract one LP from Mousetail

    !adminlp @mousetail -1

### `!admincancel [game id]` 🛡️

Cancels a game. If the game is finished refund lp. If not, set a flag that prevents if from being `!winner` from
working.

Example:

    !admincancel SilverWheat242F

### `!adminlplist` 🛡️✈️

Gets all players LP's as a CSV file. Guaranteed to work upto 500 players. Accepts the same filters as `!rank`
or `!leaderboard`.

### `!lpupload` 🛡️✈️

Adds LP to all players based google sheets document. File should have 3 columns and a header row: Discord name, (
ignored), LP

### `!adminsetting` 🛡️✈️

Sets game settings, options are:

| Setting | Meaning | 
| :----   | :------ |
| link    | The link spammed in `!rank` and `!leaderboard` commands. |
| min_wager | The minimal wager amount allowed, 0 wagers are always allowed |
| max_wager | The maximum wager allowed per person per match |
| wager_step | The wager must be a multiple of `wager_step`. Can be set to 1 to disable the step. |
| first_game_bonus | LP bonus for first game of a specfic mode every day |
| first_win_bonus | LP bonus for first win of a specific mode every day |
| **Server layout settings** | Meaning |
| report_channel | The ID for the channel reports should be sent to. |
| bot_spam_channel | The ID of the channel reserved for bot spam. Some commands work only there |
| donator_role | The role id of the donator role, displayed on the website |
| **Finals LP Settings** | Meaning |
| rank_bracket_multiplier | The multiplier of given LP given during finals for each successive bracket. Effected by rank_bracket_multiplier. |
| rank_bonus_win_semifinal | Bonus LP given to those who win a semifinal, default 0. Effected by rank_bracket_multiplier. |
| rank_bonus_win_final | Bonus LP given to whoever wins the finals. Effected by rank_bracket_multiplier |
| rank_bonus_sf_loser_bonus | Bonus LP given to whoever loses semifinals. Default 0. Effected by rank_bracket_multiplier |
| **Qualifier LP Settings** | Meaning |
| qualifier_template | Template used for qualifiers. |
| qualifier_games_per_row | How many games are placed side by side each row |
| qualifier_horizontal_distance | number of columns between the leftmost cell of one game and the left of the next |
| qualifier_vertical_distance | number of rows between the top of one game and the top of the one below it, assuming 0p games. 1 will be added per number of players per table |
| qualifier_horizontal_offset | Position of top left corner of top left game. A=0, B=1, etc. |
| qualifier_vertical_offset | Position of the top left corner of the top game. 0 indexed so subtract 1 |

To change the maximum wager to 100:

     !adminsetting max_wager 100

To set the link to this page:

     !adminsetting link https://catan-community-website-2.vercel.app/info

To disable wagers all together:

    !adminsetting max_wager 0
    !adminsetting min_wager 100

## Mod tournaments commands

    !updatescores [drive link]

Update the scores from the tournament page given by `[drive link]`

Will sum up the scores form all players in sheets containing the word "Match" and paste the results in any sheet
containing the word "ranking". Will not change formatting.

In order to edit the document, share the drive sheet with `catan-community-bot@catancommunity.iam.gserviceaccount.com`

Expects the sheet to be formatted according to the standard format which means:

* 4 tables, starting at B2, F2, J2, and N2
* Each table consits of 3 columns: Discord Username, Colonist Username, VP
* 10 VP = win
* Second row of tables starts at 9, and so forth for every row
* Ranking starts at B3, and has 10 columns:
    * Rank
    * Discord name
    * Colonist Name
    * Wins
    * Points
    * Points Percent
    * Table Points
    * Points Difference
    * Average Opponent VP
    * Total Opponent Wins

This function is intended for the scors of a single day, it will not merge scores form two different sheets or do backup
merging etc. and other function that require cross-referencing any other data source.

### `!finalslp` 🛡️

Calculate the LP's everyone gets for finals. Resulting file is suitable for `!lpupload`.

Please check the file before uploading. It is viewable by everybody but editable by mods. If you are a mod and don't
have access PM anyone who does.

### `!finalsraffle` 🛡️

Calculate the number of raffle tickets everyone gets from the finals. Returns a file in a a suitable format for the
raffle program. Number of tickets is based on the Anora distribution.

## Mod Management Commands

### `!adminrole [@role]` 🛡️✈️

Adds `role` to the list of roles permitted to use admin commands.

For example:

    !adminrole @tournamentSupport

### `!unadminrole [@role]` 🛡️✈️

Removes `role` form the list of roles permitted to use admin commands. The server owner can always use admin commands
regardless of the role settings.

## Contributing

I am just one guy and also have a full time job, so won't always be able to fix all the issues with the bot. If you find
a problem and don't want to wait for me to fix it, just do it yourself! The code is
right [here](https://gitlab.com/mousetail/catan-community-discord-bot), just submit a pull request, and if it's a good
suggestion I'll be sure to merge it in.

Current contributors:

* SupHip
* TBYNL
* mousetail
* WheatCoin
* you?
