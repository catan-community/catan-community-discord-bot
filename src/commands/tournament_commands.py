import io
import re
import traceback
from typing import Any
import typing

import aiohttp
import discord

import constants
from tournament import create_tables, scores, tournament
import functions


def get_document_id(drive_link: str) -> str:
    return drive_link.split("/")[-2]


async def update_scores(state, message, config):
    if not state.is_admin(message.author):
        await message.channel.send("You must be admin to use this command")
        return

    words = state.split_words(message.content)
    if len(words) == 2:
        vp_needed = 10
    elif len(words) == 3:
        if not (re.match(r"^\d+$", words[2])):
            await message.channel.send("Invalid victory LP")
            return
        vp_needed = int(words[2])
    else:
        await message.channel.send(
            "Usage: !updatescores [drive link] [vp to win (optional)]"
        )
        return

    drive_link = words[1]
    document_id = get_document_id(drive_link)

    try:
        await tournament.update_scores(config, document_id, state.settings, vp_needed)
    except (ValueError, AssertionError) as ex:
        traceback.print_exc()

        await message.channel.send(
            "There was an error updating the scores: \n"
            + "\n".join(str(i) for i in ex.args)
        )
        return

    await message.channel.send(
        f"Spreadsheet updated. See the result here: "
        f"The document was saved in <https://docs.google.com/spreadsheets/d/{document_id}/edit>"
    )


async def finals_lp(state: functions.BotState, message, config):
    if not state.is_admin(message.author):
        await message.channel.send("You must be admin to use this command")
        return

    words = state.split_words(message.content)
    if len(words) == 2:
        vp_needed = 10
    elif len(words) == 3:
        if not (re.match(r"^\d+$", words[2])):
            await message.channel.send("Invalid victory LP")
            return
        vp_needed = int(words[2])
    else:
        await message.channel.send(
            "Usage: !finalslp [drive link] [vp to win (optional)]"
        )
        return

    drive_link = words[1]

    async with message.channel.typing():
        try:
            sheet_id = await scores.compute_lp_for_tournament(
                sheet_id=get_document_id(drive_link),
                points_to_win=vp_needed,
                settings=state.settings,
                config=config,
            )
        except (ValueError, AssertionError) as ex:
            await message.channel.send(
                "There was an error processing the data:\n" + "\n".join(ex.args)
            )
            return

    await message.channel.send(
        f"The document was saved in <https://docs.google.com/spreadsheets/d/{sheet_id}/edit>"
    )


async def finals_raffles(
    state: functions.BotState, message: discord.Message, config: dict[str, Any]
):
    if not state.is_admin(message.author):
        await message.channel.send("You must be admin to use this command")
        return

    words = state.split_words(message.content)
    if len(words) == 2:
        vp_needed = 10
    elif len(words) == 3:
        if not (re.match(r"^\d+$", words[2])):
            await message.channel.send("Invalid victory LP")
            return
        vp_needed = int(words[2])
    else:
        await message.channel.send(
            "Usage: !finalslp [drive link] [vp to win (optional)]"
        )
        return

    drive_link = words[1]

    async with message.channel.typing():
        try:
            result = await scores.compute_raffle_for_tournament(
                sheet_id=get_document_id(drive_link), vp_to_win=vp_needed, config=config
            )
        except (ValueError, AssertionError) as ex:
            await message.channel.send(
                "There was an error processing the data:\n" + "\n".join(ex.args)
            )
            return

    file = discord.File(io.StringIO(result), filename="raffle.txt")

    await message.channel.send(f"Raffles calculated:", file=file)


async def create_tables_command(
    state: functions.BotState, message: discord.Message, config: dict[str, typing.Any]
):
    async with message.channel.typing():
        words = state.split_words(message.content)

        if len(message.role_mentions) == 1:
            role = message.role_mentions[0]
        else:
            role = discord.utils.get(message.guild.roles, id=int(words[1]))
            if role is None:
                await message.channel.send("Can't find role ", words[1])
                return

        if len(words) >= 3:
            name = words[2]
        else:
            name = "CC Open"

        if len(words) >= 4:
            if words[3] not in constants.game_mode_synonyms:
                await message.channel.send(
                    f"Can't find game mode: {words[3]}. Ensure you spelled it correctly."
                )
            expansion = constants.game_mode_synonyms[words[3]]
        else:
            expansion = None

        if len(words) >= 5:
            if not words[4].isnumeric():
                await message.channel.send(f"VP to win must be a number")
                return
            vp_to_win = int(words[4])
        elif expansion == "c&k":
            vp_to_win = 13
        else:
            vp_to_win = 10

        members = [i for i in message.guild.members if role in i.roles]

        async with aiohttp.ClientSession() as http:
            response = await http.post(
                "https://catancommunity.org/registration/api/colonist_name",
                headers={"x-authorization": config.get("website_token")},
                json=[i.id for i in members],
            )
            data = await response.json()

            try:
                sheet_id = await create_tables.create_table_sheets(
                    config,
                    name,
                    state.settings,
                    create_tables.generate_tables(
                        [
                            create_tables.Player(
                                discord_name=i.name + "#" + i.discriminator,
                                colonist_name=data.get(
                                    str(i.id), {"colonist_name": "Please Register"}
                                )["colonist_name"],
                                owns_expansion=create_tables.has_expansion(
                                    data.get(str(i.id)), expansion
                                ),
                            )
                            for i in members
                        ]
                    ),
                    vp_to_win=vp_to_win,
                )

            except (ValueError, AssertionError) as ex:
                await message.channel.send(" ".join(str(i) for i in ex.args))
                return

        await message.channel.send(
            f"Document saved in <https://docs.google.com/spreadsheets/d/{sheet_id}/edit>"
        )


async def add_round(
    state: functions.BotState, message: discord.Message, config: dict[str, typing.Any]
):
    words = state.split_words(message.content)
    spreadsheet_id = get_document_id(words[1])
    print(spreadsheet_id)
    try:
        await create_tables.duplicate_sheet(config, spreadsheet_id, state.settings)
    except (ValueError, AssertionError) as ex:
        await message.channel.send(" ".join(str(i) for i in ex.args))
        return

    await message.channel.send(
        f"Document saved in <https://docs.google.com/spreadsheets/d/{spreadsheet_id}/edit>"
    )
