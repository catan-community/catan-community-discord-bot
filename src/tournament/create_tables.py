import asyncio
import dataclasses
import json
import random
import typing

import aiohttp

from . import sheets


@dataclasses.dataclass
class Player:
    discord_name: str
    colonist_name: str
    owns_expansion: bool = False


@dataclasses.dataclass
class Table:
    players: typing.List[Player]
    name: str


def list_get(l, index, default):
    if index < len(l):
        return l[index]
    else:
        return default


def process_player(player: Player):
    return [
        player.discord_name,
        player.colonist_name,
        None,
        None,
        "*" if player.owns_expansion else None,
    ]


def generate_data(
    tables: list[Table],
    table_size: tuple[int, int, int, int],
    nrof_players=4,
) -> list[typing.Dict]:
    rows = 4
    out = []

    x = 0
    y = 0
    for table in tables:
        out.append(
            {
                "range": sheets.SheetRange.with_size(
                    table_size[0] + table_size[2] * x,
                    table_size[1] + (table_size[3] + nrof_players) * y,
                    width=table_size[2],
                    height=table_size[2] + nrof_players,
                ),
                "values": [
                    [table.name],
                    ["Discord Username", "Colonist Username", "VP"],
                    *(process_player(i) for i in table.players),
                ],
                "player_ranges": [
                    sheets.SheetRange.with_size(
                        1 + 4 * x, 3 + (3 + nrof_players) * y + i, width=3, height=1
                    )
                    for i in range(nrof_players)
                ],
            }
        )

        x += 1
        if x >= rows:
            x = 0
            y += 1
    return out


def generate_tables(
    players: typing.List[Player], players_per_table=4
) -> typing.List[Table]:
    random.shuffle(players)
    players.sort(key=lambda x: x.owns_expansion)
    tables = []

    nrof_tables = (len(players) + players_per_table - 1) // players_per_table
    for i in range(nrof_tables):
        table = Table(
            name=f"Table {i + 1}",
            players=[
                list_get(
                    players,
                    i + j * nrof_tables,
                    Player(
                        discord_name="The host will find a backup",
                        colonist_name="backup",
                    ),
                )
                for j in range(players_per_table)
            ],
        )
        tables.append(table)

    return tables


def regenerate_tables(
    previous_tables: typing.List[typing.List[Table]], players_per_table=4
):
    # tables2 = tables[:]
    tables = previous_tables[-1][:]
    # tables_previous_round = {}
    # for index, table in enumerate(previous_tables[0]):
    #    for index2, player in enumerate(table.players):
    #        tables_previous_round[player.discord_name] = index * players_per_table + index2
    # for table in tables:
    #    table.players.sort(key=lambda player: tables_previous_round.get(player.discord_name, 0))
    # tables.sort(key=lambda i: min(tables_previous_round.get(player.discord_name, 0) for player in i.players))
    # for table in tables:
    #    table.players.sort(key=lambda x: x.owns_expansion)
    # for index, i in enumerate(tables):
    #    index2= tables2.index(i)
    #    if index2 == index - 1:

    players: list[list[Player]] = [[] for _ in tables]
    for i in range(len(tables)):
        players[i].extend(
            (
                tables[(i + k) % len(players)].players[k]
                for k in range(players_per_table)
            )
        )

    # for i in players:
    #    random.shuffle(i)

    return [
        Table(name=f"Table {i + 1}", players=player) for i, player in enumerate(players)
    ]


async def create_table_sheets(
    config: dict[str, typing.Any],
    new_file_name: str,
    settings: dict[str, typing.Any],
    tables: list[Table],
    players_per_table: int = 4,
    vp_to_win: int = 10,
):
    async with aiohttp.ClientSession() as http:
        token = await sheets.get_access_token(http, config)

        template_sheet_id = settings["qualifier_template"].split("/")[-2]
        print(template_sheet_id)
        spreadsheet = await sheets.load_sheet_contents(template_sheet_id, config)
        new_sheet_id = spreadsheet.get_sheet_id(
            next(i for i in spreadsheet.get_sheets() if "match" in i.lower())
        )

        response = await http.post(
            f"https://www.googleapis.com/drive/v3/files/{template_sheet_id}/copy",
            headers={"Authorization": "Bearer " + token},
            json={"name": new_file_name},
        )

        data = await response.json()
        response.raise_for_status()
        spreadsheet_id = data["id"]

        await sheets.share_document(http, spreadsheet_id, token)
        update_data = generate_data(
            tables,
            (
                settings["qualifier_horizontal_offset"],
                settings["qualifier_vertical_offset"],
                settings["qualifier_horizontal_distance"],
                settings["qualifier_vertical_distance"],
            ),
            nrof_players=players_per_table,
        )

        result = await http.post(
            f"https://sheets.googleapis.com/v4/spreadsheets/{spreadsheet_id}:batchUpdate",
            headers={"Authorization": "Bearer " + token},
            json={
                "includeSpreadsheetInResponse": False,
                "requests": [
                    *(
                        {
                            "copyPaste": {
                                "source": sheets.SheetRange.with_size(
                                    x=settings["qualifier_horizontal_offset"],
                                    y=settings["qualifier_vertical_offset"],
                                    width=settings["qualifier_horizontal_distance"],
                                    height=settings["qualifier_vertical_distance"]
                                    + players_per_table,
                                ).to_grid_range(new_sheet_id),
                                "destination": i["range"].to_grid_range(new_sheet_id),
                                "pasteType": "PASTE_FORMAT",
                            }
                        }
                        for i in update_data
                    ),
                    *(
                        {
                            "updateCells": {
                                "range": i["range"].to_grid_range(new_sheet_id),
                                "fields": "userEnteredValue",
                                "rows": [
                                    {
                                        "values": [
                                            sheets.create_cell_data(cell)
                                            for cell in row
                                        ]
                                    }
                                    for row in i["values"]
                                ],
                            }
                        }
                        for i in update_data
                    ),
                    #         *(
                    #             {
                    #                 "addConditionalFormatRule": {
                    #                     "rule": {
                    #                         "ranges": [
                    #                             player_range.to_grid_range(new_sheet_id)
                    #                         ],
                    #                         "booleanRule": {
                    #                             "condition": {
                    #                                 "type": "CUSTOM_FORMULA",
                    #                                 "values": [
                    #                                     {
                    #                                         "userEnteredValue": "="
                    #                                         + sheets.coords_to_cell(
                    #                                             (
                    #                                                 player_range.x_start + 2,
                    #                                                 player_range.y_start,
                    #                                             ),
                    #                                             fixed_x=True,
                    #                                             fixed_y=True,
                    #                                         )
                    #                                         + ">="
                    #                                         + str(vp_to_win)
                    #                                     }
                    #                                 ],
                    #                             },
                    #                             "format": {
                    #                                 "backgroundColor": {
                    #                                     "red": 0,
                    #                                     "green": 1,
                    #                                     "blue": 0,
                    #                                     "alpha": 1,
                    #                                 }
                    #                             },
                    #                         },
                    #                     },
                    #                     "index": 0,
                    #                 }
                    #             }
                    #             for table in update_data
                    #             for player_range in table["player_ranges"]
                    #         ),
                ],
            },
        )
        print(await result.json())
        result.raise_for_status()
        return spreadsheet_id


async def duplicate_sheet(
    config: dict[str, typing.Any],
    spreadsheet_id: str,
    settings: dict[str, typing.Any],
    nrof_players_per_table: int = 4,
):
    spreadsheet = await sheets.load_sheet_contents(spreadsheet_id, config)

    highest_number = -1
    highest_sheet = None
    for sheet_name in spreadsheet.get_sheets():
        print(sheet_name)
        if "match" in sheet_name.lower():
            sheet_number = int(sheet_name.split()[-1])
            if sheet_number > highest_number:
                highest_number = sheet_number
                highest_sheet = sheet_name

    if highest_sheet is None:
        raise ValueError("No previous sheet found in document")

    # players = []

    # print(highest_sheet)
    # for game_info in sheets.get_qualifier_game_info(spreadsheet, highest_sheet):
    #    for player in game_info.players:
    #        players.append(
    #            Player(
    #                discord_name=player.discord_name, colonist_name=player.colonist_name,
    #                owns_expansion=player.owns_expansion
    #           )
    #        )

    tables = regenerate_tables(
        [
            [
                Table(
                    name=game.name,
                    players=[
                        Player(
                            discord_name=player.discord_name,
                            colonist_name=player.colonist_name,
                            owns_expansion=player.owns_expansion,
                        )
                        for player in game.players
                    ],
                )
                for game in sheets.get_qualifier_game_info(
                    spreadsheet, sheet, settings, nrof_players_per_table
                )
            ]
            for sheet in spreadsheet.get_sheets()
            if "match" in sheet.lower()
        ]
    )

    async with aiohttp.ClientSession() as http:
        token = await sheets.get_access_token(http, config)
        new_sheet_id = spreadsheet.get_new_sheet_id()

        update_data = generate_data(
            tables,
            (
                settings["qualifier_horizontal_offset"],
                settings["qualifier_vertical_offset"],
                settings["qualifier_horizontal_distance"],
                settings["qualifier_vertical_distance"],
            ),
            nrof_players=nrof_players_per_table,
        )
        result = await http.post(
            f"https://sheets.googleapis.com/v4/spreadsheets/{spreadsheet_id}:batchUpdate",
            headers={"Authorization": "Bearer " + token},
            json={
                "includeSpreadsheetInResponse": False,
                "requests": [
                    {
                        "duplicateSheet": {
                            "sourceSheetId": spreadsheet.get_sheet_id(highest_sheet),
                            "insertSheetIndex": spreadsheet.sheets[highest_sheet][
                                "properties"
                            ]["index"]
                            + 1,
                            "newSheetName": f"Q Match {highest_number + 1}",
                            "newSheetId": new_sheet_id,
                        }
                    },
                    *(
                        {
                            "updateCells": {
                                "range": i["range"].to_grid_range(new_sheet_id),
                                "fields": "userEnteredValue",
                                "rows": [
                                    {
                                        "values": [
                                            sheets.create_cell_data(cell)
                                            for cell in row
                                        ]
                                    }
                                    for row in i["values"]
                                ],
                            }
                        }
                        for i in update_data
                    ),
                ],
            },
        )
        data = await result.json()
        print(data)
        result.raise_for_status()


def has_expansion(player, expansion):
    if expansion is None:
        return True
    if player is None:
        return False
    if player["expansions"].get(expansion):
        return True
    return False


if __name__ == "__main__":
    players = [
        Player(discord_name="Mousetail#2544", colonist_name="mousetail"),
        Player(discord_name="Athanais#2843", colonist_name="Athanais"),
        Player(discord_name="Karm#1234", colonist_name="Karmu"),
        Player(discord_name="Puzzles#8942", colonist_name="Pzzles"),
        Player(discord_name="Hippo#2432", colonist_name="SupHip"),
    ]

    tables = generate_tables(players, 4)

    with open("../../secret.json") as f:
        config = json.load(f)

    # asyncio.run(create_table_sheets(config, "Name", tables))
    # asyncio.run(duplicate_sheet(config, "17_vLQ8Rv5bYxWvfsuhCNZTzZMUg0hXbYqJ9ts7nOa-k"))
