# Rationale

The current databae format has a couple of problems:

* Filtering for more complex categories is a pain
* Could run into the maximum size for each record
* Lack of standardization makes errors easy
* Adding more categories is also hard, because it would require a database migration every time
* Adding more fields is a bit complex because the same set of fields is stored in 5 different places.

Thus it would be better to do one migration to solve all these issues.

# Basic Tables

The new system will have a seperate record for every category, rather than attempting to store them all in the user
table.

* User -> Basic metadata about a user (name, colonist name). Specifically has *no* lp.
* Match -> Basic info on a match. Includes players, confirmed or not, created date *(new)*. Also contains the category
  the game belongs in.
* A tournament, contains the date, the type, and the name.
* A user LP. Has these columns:
    * User
    * Year
    * Month
    * Type: A tournament or a MM game. Others might be added later.
    * Players: The number of players in the game
    * Mode: The game mode, base, seafarers, or C&K
    * Map: The map
    * Tournament: For tournaments, a foreign key to tournament
    * LP: The total LP earned for games matching these conditions.
    * Games: The total games played in this category
    * Normalized wins: Total players defeated + Total wins. Always equals Players * Wins

# Calculating LP's for a specific category

This will be done with a slightly complex aggregation. Shouldn't be to much of a problem.

By default the filter will be:

* Current month
* Current year
* Any number of players
* Any mode
* Any map

# Specifiying a specific filter

You can filter by any number of categories, for example:

```text
!rank month c&k 4p
!rank january usa
!leaderboard 2020
!rank c&k
!rank year boatsboatsboats
```

The same filters could be applied on the website more easily too.

# Performance considerations

One disadvantage of this method is that everything must be done with an aggregation. Every aggregation must loop over
all people and join possibly 10 or so different LP's, each time you want to calculate someone's rank. One option would
be to do some caching for the leaderboard in common categories, but still `!rank` might be slow with many people.

# Changes needed to make this work

## Code updates

Finding the game mode and map is not immediately neccecairy, might start with just time, and number of players. In that
case only the `confirmGame` and `cancelGame` methods need to be updated.

There also need to be changes to how `!adminlp` works to properly add lp to the correct category.

## Database updates

The database migration is a challanging task. It needs to be deployed simultaneously with the code update. The code
changes are too significant to make a overlapping system like I did with the game IDs.

Thus, what I need is:

* A *tested* migration script, to update the database
* A check to see if using the old database format
* Migrate the database if it's old

The migration is quite complex so should ideally be tested first. Maybe keep the old tables a little while while I check
if the new data is correct? - feasable -

What concretely needs to happen that for every user, the `history` field needs to be moved to a separate table. Assume
all games are base 4p for now.

Tournament code needs to be updated to store the correct date for tournaments.

# Other much needed changes

* Use the bot framework to split every command into a seperate function
* Allow `/` commands
* Split into multiple files
    * Remove all the top-level files and make sure everything is inside a folder
* Add season 1 scores
